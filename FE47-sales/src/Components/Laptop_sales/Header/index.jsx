import React, { Component } from 'react';
import { AppBar, Toolbar, Typography, Button, IconButton } from '@material-ui/core';
class Header extends Component {
    render() {
        return (
            <div>
                <AppBar position="static" color="inherit" >
                    <Toolbar>
                        <Typography variant="h6" color="inherit" style={{ flexGrow: 1, textAlign: "left" }}>
                            CyberSoft
                        </Typography>
                        <Button color="primary" className="mr-3">
                            Home
                        </Button>
                        <Button color="secondary" className="mr-3">
                            Contact
                        </Button>
                        <Button color="secondary" className="mr-3">
                            About
                        </Button>
                        <Button color="secondary" className="mr-3">
                            Login
                        </Button>
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

export default Header;