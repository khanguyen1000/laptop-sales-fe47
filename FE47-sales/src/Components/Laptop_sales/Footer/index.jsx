import React, { Component } from 'react';
import imgPromotion from '../../../img/promotion_1.png';
import imgPromotion2 from '../../../img/promotion_2.png';
import imgPromotion3 from '../../../img/promotion_3.jpg';
class Footer extends Component {
    render() {
        return (
            <div className="bg-dark pb-5">
                <div className="container">
                    <p className="m-0 py-5 text-white h3">PROMOTION</p>
                    <div className="bg-light row">
                        <div className="col-4 py-2">
                            <img src={imgPromotion} style={{ maxWidth: '100%' }} />
                        </div>
                        <div className="col-4 py-2">
                            <img src={imgPromotion2} style={{ maxWidth: '100%' }} />
                        </div>
                        <div className="col-4 py-2">
                            <img src={imgPromotion3} style={{ maxWidth: '100%' }} />
                        </div>
                    </div>

                </div>


            </div>
        );
    }
}

export default Footer;