import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
class Product extends Component {
    render() {
        const { name, img, desc, backCamera } = this.props.item;
        return (
            <div>
                <Card style={{ maxWidth: '300px' }}>
                    <CardActionArea>
                        <CardMedia
                            style={{ height: '300px' }}
                            image={img}
                            title={backCamera}
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                {name}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                {desc}
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                    <CardActions className="justify-content-center">
                        <Button variant="outlined" size="small" color="primary">
                            Detail
                         </Button>
                        <Button variant="outlined" size="small" color="secondary">
                            Cart
                        </Button>
                    </CardActions>
                </Card>
            </div>
        );
    }
}

export default Product;