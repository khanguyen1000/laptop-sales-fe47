import React, { Component } from 'react';
import Slider1img from '../../../img/slide_1.jpg';
import Slider2img from '../../../img/slide_2.jpg';
import Slider3img from '../../../img/slide_3.jpg';
class SliderComponent extends Component {
    render() {
        return (
            <div>
                <div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
                    <ul className="carousel-indicators">
                        <li data-target="#carouselExampleControls" data-slide-to={0} className="active" />
                        <li data-target="#carouselExampleControls" data-slide-to={1} />
                        <li data-target="#carouselExampleControls" data-slide-to={2} />
                    </ul>
                    <div className="carousel-inner">
                        <div className="carousel-item active">
                            <img className="d-block w-100 h-75 img-fluid" src={Slider2img} alt="First slide" />
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100 h-75 img-fluid" src={Slider1img} alt="Second slide" />
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100 h-75 img-fluid" src={Slider3img} alt="Third slide" />
                        </div>
                    </div>
                    <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true" />
                        <span className="sr-only">Previous</span>
                    </a>
                    <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true" />
                        <span className="sr-only">Next</span>
                    </a>
                </div>
            </div>
        );
    }
}

export default SliderComponent;