import React, { Component } from 'react';
import imageBg from '../glassesImage/background.jpg';
import modelImg from '../glassesImage/model.jpg';
import glass from '../glassesImage/v1.png';
import './index.css';

let arrProduct = [

    { id: 1, price: 30, name: 'GUCCI G8850U', url: require('../glassesImage/v1.png'), desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 2, price: 50, name: 'GUCCI G8759H', url: require('../glassesImage/v2.png'), desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 3, price: 30, name: 'DIOR D6700HQ', url: require('../glassesImage/v3.png'), desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 4, price: 30, name: 'DIOR D6005U', url: require('../glassesImage/v4.png'), desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 5, price: 30, name: 'PRADA P8750', url: require('../glassesImage/v5.png'), desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 6, price: 30, name: 'PRADA P9700', url: require('../glassesImage/v6.png'), desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 7, price: 30, name: 'FENDI F8750', url: require('../glassesImage/v7.png'), desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 8, price: 30, name: 'FENDI F8500', url: require('../glassesImage/v8.png'), desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

    { id: 9, price: 30, name: 'FENDI F4300', url: require('../glassesImage/v9.png'), desc: 'Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ' },

];

class Home extends Component {
    state = {
        listGlasses: arrProduct,
        name: arrProduct[0].name,
        desc: arrProduct[0].desc,
        glass: glass
    }

    clickEventChange = (n, d, u) => () => {
        this.setState({
            name: n,
            desc: d,
            glass: u
        });
    }
    renderProduct = () => {
        return this.state.listGlasses.map((item, index) => {
            return (
                <div key={index} >
                    <img src={item.url} alt="" className="img-fluid item-g" onClick={this.clickEventChange(item.name, item.desc, item.url)} />
                </div>
            )
        })
    }
    render() {
        return (
            <div>
                <div className="wap">
                    <div className="content">
                        <p>TRY GLASSES APP ONLINE</p>
                        <div className="glassesGroup">
                            <div className="glass">
                                <img src={modelImg} alt="" className="img-fluid modalImg" />
                                <div className="testGlass">
                                    <img src={this.state.glass} className="img-fluid img_v" />
                                    <div className="G_content">
                                        <p className="name">{this.state.name}</p>
                                        <p className="desc">{this.state.desc}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="glass"><img src={modelImg} alt="" className="img-fluid modalImg" /></div>
                        </div>
                        <div className="listGlasses">
                            {this.renderProduct()}
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default Home;